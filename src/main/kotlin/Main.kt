import cv.*
import template.Stockholm
import java.io.File

fun main() {

    val firstName = "Martin"
    val middleNames = mutableListOf<String>(

    )
    val lastName = "Gaens"
    val targetProfession = "Programmer"
    val profile = """
        Passionate hobby programmer with 5+ years of hobby programming experience who likes to
        learn new things.
        """.trimIndent()
    val employments = mutableListOf(
        Employment(
            "Skiing instructor",
            "Patty Ski, Donovaly, Slovakia",
            TimeRange("December 2018", "January 2019"),
            "Taught kids and adults how to ski."
        ),
        Employment(
            "Taxi driver",
            "Eko Taxi s.r.o, Zvolen, Slovakia",
            TimeRange("June 2020", "August 2020"),
            "Driving a vehicle while transporting passengers from one place to another."
        ),
        Employment(
            "UPS Parcel Sorter",
            "Randstad, Hasselt, Belgium",
            TimeRange("July 2021", "August 2021"),
            "Summer night job for one month in which I sorted parcels."
        ),
        Employment(
            "Hotel room cleaner",
            "N-Clean, Hämeenlinna, Finland",
            TimeRange("September 2021", "Present"),
            "Cleaning hotel rooms for new customers."
        )
    )
    val educations = mutableListOf(
        Education(
            "Grammar School Jozefa Gregora Tajovského, Banská Bystrica, Slovakia",
            TimeRange("September 2016", "May 2021"),
            "Finished the French bilingual section of the school at the C1 level of French."
        ),
        Education(
            "Bachelor of Business Administration (BBA), Computer Applications degree, Hämeenlinna, Finland",
            TimeRange("August 2021", "Present"),
            "Started university in Finland this year."
        )
    )
    val hobbies = """
        Hobby programming is the biggest part of my programming life. I started 'programming' when I was 11, and it has
        stuck with me ever since. I've worked with HTML, CSS and Javascript on some remote-controlled lighting project.
        I've used the Processing 3 library in Java and later P5.js to create programmatic art. I've worked with Raspberry
        Pis, Arduinos and ESP8266 microprocessors to create Home Assistant home automation projects. And there's much more.
        Other than programming, my hobbies include snowboarding, playing chess, playing piano, drums and guitar, listening
        to music and playing videogames.
    """.trimIndent()
    val skills = mutableListOf(
        ProgressBar("Git", "Intermediate", 6),
        ProgressBar("Kotlin", "Experienced", 10),
        ProgressBar("Java", "Advanced", 7),
        ProgressBar("HTML", "Advanced", 7),
        ProgressBar("JavaScript", "Intermediate", 4),
        ProgressBar("Python 3", "Intermediate", 5),
        ProgressBar("C++", "Intermediate", 4),
        ProgressBar("Android", "Intermediate", 4)
    )
    val languages = mutableListOf(
        ProgressBar("Slovak", "C2", 10),
        ProgressBar("Czech", "C1", 9),
        ProgressBar("English", "C1", 9),
        ProgressBar("Dutch", "B2", 7),
        ProgressBar("French", "C1", 9)
    )

    val cv = CV(
        firstName,
        middleNames,
        lastName,
        targetProfession,
        profile,
        employments,
        educations,
        hobbies,
        skills,
        languages
    )

    Stockholm.generateHTML(cv, File("public/my-main-cv/index.html"))

    println(cv)

}