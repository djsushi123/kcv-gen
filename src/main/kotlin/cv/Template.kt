package cv

import java.io.File

abstract class Template {

    abstract fun generateHTML(cv: CV, file: File)

}