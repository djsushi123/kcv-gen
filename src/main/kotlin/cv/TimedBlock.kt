package cv

open class TimedBlock(
    var title: String,
    open var timeRange: TimeRange,
    open var description: String
) : Block()