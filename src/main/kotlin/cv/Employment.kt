package cv

data class Employment(
    var job: String,
    var employer: String,
    override var timeRange: TimeRange,
    override var description: String
) : TimedBlock(
    "$job at $employer",
    timeRange,
    description
)