package cv

data class Education(
    var schoolName: String,
    override var timeRange: TimeRange,
    override var description: String
) : TimedBlock(
    schoolName,
    timeRange,
    description
)
