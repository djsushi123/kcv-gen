package cv

// TODO implement a better way to store datetime, something that can also hold very non-specific times like just the
// year but can also hold a specific date in case someone needs to be this specific
data class TimeRange(var startTime: String, var endTime: String) {

    override fun toString(): String {
        return "$startTime - $endTime"
    }

}
