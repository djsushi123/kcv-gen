package cv

data class CV(
    var firstName: String,
    var middleNames: MutableList<String> = mutableListOf(),
    var lastName: String,
    var targetProfession: String,
    var profile: String,
    var employmentHistory: MutableList<Employment>,
    var education: MutableList<Education>,
    var hobbies: String,
    var skills: MutableList<ProgressBar>,
    var languages: MutableList<ProgressBar>
) {
    val fullName
        get() = "$firstName ${middleNames.joinToString(" ")} $lastName"
}