package cv

class ProgressBar(var title: String, var textLevel: String, var progress: Int) : Block() {

    init {
        if (progress > 10) progress = 10
        if (progress < 0) progress = 0
    }

}
