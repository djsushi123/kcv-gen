package template

import cv.*
import kotlinx.html.*
import kotlinx.html.stream.createHTML
import util.path
import java.io.File

object Stockholm : Template() {

    override fun generateHTML(cv: CV, file: File) {

        val generated = createHTML().html {
            lang = "en"

            head {
                meta { charset = "UTF-8" }
                title("Generated CV")

                // bootstrap
                link(
                    "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css",
                    "stylesheet"
                )
                link("css/styles.css", "stylesheet")
            }

            body {
                div("container-xl rounded paper") {
                    div("row info-header") {
                        div("col-12") {
                            span("full-name") { +cv.fullName }
                            br
                            span("target-profession") { +cv.targetProfession }
                            br
                            br
                        }
                    }
                    div("row") {
                        div("col-9") {
                            generateSection(
                                "Profile",
                                Icon.PROFILE,
                                mutableListOf(
                                    TextBlock(cv.profile)
                                )
                            )
                            generateSection(
                                "Employment History",
                                Icon.WORK,
                                cv.employmentHistory
                            )
                            generateSection(
                                "Education",
                                Icon.EDUCATION,
                                cv.education
                            )
                            generateSection(
                                "Hobbies",
                                Icon.HOBBY,
                                mutableListOf(
                                    TextBlock(cv.hobbies)
                                )
                            )
                        }
                        div("col-3") {
                            generateDetailsSection(
                                "Details",
                                "Visamäentie 25 F 59 a\nHämeenlinna 13100\nFinland\n+421904868240\ndjsushi123@gmail.com\nhttps://gitlab.com/djsushi123",
                                "Slovakian",
                                "B1",
                                "11. September 2001",
                                "Hasselt, Belgium"
                            )
                            generateLittleProgressSection("Skills", cv.skills)
                            generateLittleProgressSection("Languages", cv.languages)
                        }
                    }
                    span {
                        br
                        +"This CV was generated using my own Kotlin CV generator, available at "
                        a(
                            "https://gitlab.com/djsushi123/kcv-gen",
                            "_blank"
                        ) { +"https://gitlab.com/djsushi123/kcv-gen" }
                    }
                }
                script {
                    src = "https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
                    attributes["crossorigin"] = "anonymous"
                }
            }
        }

        file.writeText(generated)
        // debugging
        println(generated)
    }

    private fun HtmlBlockTag.generateDetailsSection(
        title: String, text: String,
        nationality: String, drivingLicense: String,
        dateOfBirth: String, placeOfBirth: String
    ) {
        div("row little-section") {
            div("col-auto") {
                h5("title") { +title }
                text.split("\n").forEach {
                    if (it.contains("@"))
                        a("mailto:$it", classes = "email") { +it }
                    else if (it.contains("gitlab")) {
                        a("https://gitlab.com/djsushi123", "_blank") {
                            style = "font-weight: bold;"
                            +"GitLab"
                        }
                    } else
                        span("text") { +it }
                    br
                }
                br
                span("subtitle") { +"Nationality" }
                br
                span("text") { +nationality }
                br;br
                span("subtitle") { +"Driving license" }
                br
                span("text") { +drivingLicense }
                br;br
                span("subtitle") { +"Date/place of birth" }
                br
                span("text") { +dateOfBirth; br; +placeOfBirth }
            }
        }
    }

    private fun HtmlBlockTag.generateLittleSection(title: String, text: String) {
        div("row little-section") {
            div("col-auto") {
                span("title") { +title }
                span("text") { +text }
            }
        }
    }

    private fun HtmlBlockTag.generateLittleProgressSection(title: String, progressBars: List<ProgressBar>) {
        div("row little-section little-progress-section") {
            div("col-10") {
                h5 { +title }
                progressBars.forEach {
                    div("row progress-row") {
                        div("col-6") {
                            span("title") { +it.title }
                        }
                        div("col-6 level-description") {
                            span("level") { +it.textLevel }
                        }
                    }
                    div("row") {
                        div("col-12") {
                            div("progress") {
                                style = "height: 8px;"
                                div("progress-bar") {
                                    style = "width: ${it.progress * 10}%;"
                                    role = "progressbar"
                                    attributes["aria-valuenow"] = it.progress.toString()
                                    attributes["aria-valuemin"] = "0"
                                    attributes["aria-valuemax"] = "10"
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private fun HtmlBlockTag.generateSection(title: String, icon: Icon, blocks: MutableList<out Block>) {
        div("row section") {
            div("col-auto") {
                svg("icon") {
                    attributes["width"] = "23"
                    attributes["fill"] = "currentColor"
                    attributes["viewBox"] = "0 0 16 16"

                    icon.paths.forEach {
                        path {
                            attributes["d"] = it
                        }
                    }

                }
            }
            div("col") {
                h4 {
                    style = "font-weight: bold;"
                    +title
                }
                blocks.forEach {
                    when (it) {
                        is TimedBlock -> generateTimedBlock(it)
                        is TextBlock -> generateTextBlock(it)
                    }
                }
            }
        }
    }

    private fun HtmlBlockTag.generateTimedBlock(block: TimedBlock) {
        div("row block timed-block") {
            span("title") { +block.title }
            span("date") { +block.timeRange.toString() }
            span("description") { +block.description }
        }
    }

    private fun HtmlBlockTag.generateTextBlock(block: TextBlock) {
        div("row block text-block") {
            span("text") { +block.text }
        }
    }


}