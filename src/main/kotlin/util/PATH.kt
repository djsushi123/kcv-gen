package util

import cv.Icon
import kotlinx.html.*

class PATH(consumer: TagConsumer<*>) :
    HTMLTag("path", consumer, emptyMap(),
        inlineTag = true,
        emptyTag = false), HtmlInlineTag {
}

fun SVG.path(block: PATH.() -> Unit = {}) {
    PATH(consumer).visit(block)
}

fun HtmlBlockTag.icon(icon: Icon) {
    PATH(consumer).visit { }
}