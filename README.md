# Kotlin CV Generator

The Kotlin CV Generator (short kcv-gen) is a tool designed to quickly programmatically generate a stunning CV in the
form of an HTML website.

My CV link is [here](https://djsushi123.gitlab.io/kcv-gen/my-main-cv/).

## Disclaimer

```
This tool is still under active development, and is currently not usable and suitable for anyone looking to generate 
a CV. I created this tool because I needed a way to prove my programming skills and create my CV as quickly as 
possible. However, even after utilizing the CV generated for my own job applications, I will still be working on 
this tool to make it usable by others.
```

